/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.recyclerviewswipedismiss;

import org.junit.Assert;
import org.junit.Test;

/**
 * DecimalUtilsTest
 *
 * @author:username
 * @since 2021-05-25
 */
public class DecimalUtilsTest {

    @Test
    public void add() {
        Float result = DecimalUtils.add(111111.12f, 666666.29f);
        Assert.assertEquals("相等吗", new Float(777777.41), result);
    }

    @Test
    public void sub() {
        Float result = DecimalUtils.sub(345.69f, 123.45f);
        Assert.assertEquals("相等吗", new Float(222.24), result);
    }

    @Test
    public void mul() {
        Float result = DecimalUtils.mul(86.5f, 11.2f);
        Assert.assertEquals("相等吗", new Float(968.8), result);
    }

}
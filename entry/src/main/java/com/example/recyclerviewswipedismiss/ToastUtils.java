/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.recyclerviewswipedismiss;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.window.dialog.ToastDialog;

/**
 * ToastUtils
 *
 * @since 2021-04-20
 */
public final class ToastUtils {
    private static final int DURATION = 500;
    private static ToastDialog toast;

    private ToastUtils() {
    }

    /**
     * show
     *
     * @param context
     * @param text
     */
    public static void show(AbilitySlice context, String text) {
        showToast(context, text);
    }

    private static void showToast(AbilitySlice context, String text) {
        if (toast == null) {
            toast = new ToastDialog(context);
        } else {
            toast.setText(text);
            toast.setDuration(DURATION);
        }
        toast.show();
    }
}

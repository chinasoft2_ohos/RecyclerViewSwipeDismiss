/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.recyclerviewswipedismiss.slice;

import com.example.recyclerviewswipedismiss.ResourceTable;
import com.example.recyclerviewswipedismiss.SampleItem;
import com.example.recyclerviewswipedismiss.SampleItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * 测试功能界面
 *
 * @author:hello
 * @since 2021-04-25
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final int COUNT = 100;

    /**
     * onStart
     *
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initViews();
    }

    /**
     * initViews
     */
    private void initViews() {
        ArrayList<SampleItem> items = new ArrayList<>();
        for (int ii = 0; ii < COUNT; ii++) {
            items.add(new SampleItem("item" + ii + ""));
        }
        ArrayList<SampleItem> items2 = new ArrayList<>();
        for (int ii = 0; ii < COUNT; ii++) {
            items2.add(new SampleItem("item" + ii + ""));
        }
        ListContainer listContainer1 = (ListContainer) findComponentById(ResourceTable.Id_list_container1);
        ListContainer listContainer2 = (ListContainer) findComponentById(ResourceTable.Id_list_container2);
        SampleItemProvider itemProvider = new SampleItemProvider(this, items, false, true);
        SampleItemProvider itemProvider2 = new SampleItemProvider(this, items2, true, true);
        listContainer1.setItemProvider(itemProvider);
        listContainer1.setOrientation(Component.VERTICAL);
        listContainer2.setItemProvider(itemProvider2);
        listContainer2.setOrientation(Component.HORIZONTAL);
        listContainer1.setLongClickable(false);
        listContainer2.setLongClickable(false);
        listContainer1.enableScrollBar(Component.VERTICAL, true);
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

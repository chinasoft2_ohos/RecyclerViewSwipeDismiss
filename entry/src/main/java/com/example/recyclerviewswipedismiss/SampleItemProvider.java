/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.recyclerviewswipedismiss;

import io.github.codefalling.recyclerviewswipedismiss.ResUtils;
import io.github.codefalling.recyclerviewswipedismiss.SwipeDismissRecyclerViewTouchListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.CommonDialog;

import java.util.List;
import java.util.Optional;

/**
 * SampleItemProvider
 *
 * @since 2021-04-25
 */
public class SampleItemProvider extends BaseItemProvider {
    private static final float COEFFICIENT = 0.8f;

    private AbilitySlice slice;
    private List<SampleItem> list;
    private SwipeDismissRecyclerViewTouchListener listener;
    private boolean isVertical;
    private boolean isSwiper;

    /**
     * onStart
     *
     * @param sli
     * @param li
     * @param isVerti
     * @param isSwipe
     */
    public SampleItemProvider(AbilitySlice sli, List<SampleItem> li, boolean isVerti, boolean isSwipe) {
        this.slice = sli;
        this.list = li;
        this.isVertical = isVerti;
        this.isSwiper = isSwipe;
    }

    /**
     * getCount
     *
     * @return count
     */
    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    /**
     * getCount
     *
     * @param pos
     * @return obj
     */
    @Override
    public Object getItem(int pos) {
        if (list != null && pos >= 0 && pos < list.size()) {
            return list.get(pos);
        }
        return Optional.empty();
    }

    /**
     * setIsVertical
     *
     * @param pos
     * @return pos
     */
    @Override
    public long getItemId(int pos) {
        return pos;
    }

    /**
     * setIsVertical
     *
     * @param pos
     * @param component
     * @param componentContainer
     * @return cpt
     */
    @Override
    public Component getComponent(int pos, Component component, ComponentContainer componentContainer) {
        final Component cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_sample, null, false);
        ShapeElement element = new ShapeElement(slice, ResourceTable.Graphic_text_button_normal);
        ShapeElement element2 = new ShapeElement(slice, ResourceTable.Graphic_text_button_press);

        SampleItem sampleItem = list.get(pos);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        text.setText(sampleItem.getName());
        listener = new SwipeDismissRecyclerViewTouchListener.Builder(pos, component, callbacks -> {
            ToastUtils.show(slice, "删除" + text.getText());
            list.remove(pos);
            notifyDataChanged();
        }).setBackgroundId(element, element2)
                .setIsVertical(isVertical)
                .setItemClickCallback(position -> {
                    showDialog(sampleItem.getName());
                })
                .create();
        listener.setEnabled(isSwiper);
        cpt.setTouchEventListener(listener);
        return cpt;
    }

    private void showDialog(String str) {
        CommonDialog comDialog = new CommonDialog(slice);
        Component dialogView = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_layout_dialog, null, true);
        comDialog.setContentCustomComponent(dialogView);
        Text tvIndex = (Text) dialogView.findComponentById(ResourceTable.Id_tv_item);
        tvIndex.setText("Click " + str);
        CommonDialog dialog = comDialog.setContentCustomComponent(dialogView);
        dialog.setSize((int) (ResUtils.getScreenWidth(slice) * COEFFICIENT),
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        dialog.setTransparent(false);
        dialog.setAutoClosable(true);
        dialog.show();
    }
}

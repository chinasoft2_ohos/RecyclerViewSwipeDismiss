/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.recyclerviewswipedismiss;

/**
 * SampleItem
 *
 * @since 2021-04-25
 */
public class SampleItem {
    private String name;

    /**
     * SampleItem
     *
     * @param nam
     */
    public SampleItem(String nam) {
        this.name = nam;
    }

    /**
     * getName
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setName
     *
     * @param nam
     */
    public void setName(String nam) {
        this.name = nam;
    }
}

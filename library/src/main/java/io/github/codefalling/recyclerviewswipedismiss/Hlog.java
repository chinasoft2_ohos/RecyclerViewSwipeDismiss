/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.codefalling.recyclerviewswipedismiss;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log工具类
 *
 * @since 2021-04-16
 */
public final class Hlog {
    private static final String LOG_FORMAT = "%{public}s:%{public}s";
    private static final String TAG = "HLog-aaaa";
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000f00, "dqh");

    private Hlog() {
    }

    /**
     * debug
     *
     * @param tag
     * @param info
     */
    public static void d(String tag, String info) {
        HiLog.debug(LABEL_LOG, LOG_FORMAT, tag, info);
    }

    /**
     * debug
     *
     * @param info
     */
    public static void d(String info) {
        d("SLog-aaaa", info);
    }

    /**
     * debug
     *
     * @param info
     */
    public static void d(int info) {
        d("SLog-aaaa", info + "");
    }

    /**
     * error
     *
     * @param tag
     * @param info
     */
    public static void e(String tag, String info) {
        HiLog.error(LABEL_LOG, LOG_FORMAT, tag, info);
    }

    /**
     * error
     *
     * @param info
     */
    public static void e(String info) {
        e(TAG, info);
    }

    /**
     * error
     *
     * @param info
     */
    public static void e(int info) {
        e(TAG, info + "");
    }

    /**
     * error
     *
     * @param info
     */
    public static void sop(String info) {
        e(TAG, info + "");
    }
}

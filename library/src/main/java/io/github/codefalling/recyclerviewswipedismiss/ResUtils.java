/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.codefalling.recyclerviewswipedismiss;

import ohos.app.Context;

/**
 * 资源工具类
 *
 * @since 2021-04-25
 */
public final class ResUtils {
    private static final String TAG = "ResUtil";
    private static final float CONSTANT = 160;

    private ResUtils() {
    }

    /**
     * vp转px
     *
     * @param context 上下文
     * @param vp      vp
     * @return px
     */
    public static float vpToPx(Context context, float vp) {
        return context.getResourceManager().getDeviceCapability().screenDensity / CONSTANT * vp;
    }

    /**
     * 获取屏幕的宽
     *
     * @param context 上下文
     * @return 屏幕的宽，单位px
     */
    public static float getScreenWidth(Context context) {
        return vpToPx(context, context.getResourceManager().getDeviceCapability().width);
    }
}

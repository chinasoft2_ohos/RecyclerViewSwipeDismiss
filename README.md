﻿# RecyclerViewSwipeDismiss

## 项目介绍

- 项目名称：RecyclerViewSwipeDismiss
- 所属系列：openharmony的第三方组件适配移植
- 功能：水平、垂直方向滑动删除，设置不同状态背景
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v1.1.3

## 安装教程

1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
 dependencies {
   implementation 'com.gitee.chinasoft_ohos:RecyclerViewSwipeDismiss:1.0.2'
    ......  
 }
```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行,如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
   1.MainAbilitySlice中代码
   ArrayList<SampleItem> items = new ArrayList<>();
   for (int ii = 0; ii < COUNT; ii++) {
       items.add(new SampleItem("item" + ii + ""));
   }
   ArrayList<SampleItem> items2 = new ArrayList<>();
   for (int ii = 0; ii < COUNT; ii++) {
       items2.add(new SampleItem("item" + ii + ""));
   }
   ListContainer listContainer1 = (ListContainer) findComponentById(ResourceTable.Id_list_container1);
   ListContainer listContainer2 = (ListContainer) findComponentById(ResourceTable.Id_list_container2);
   SampleItemProvider itemProvider = new SampleItemProvider(this, items, false, true);
   SampleItemProvider itemProvider2 = new SampleItemProvider(this, items2, true, true);
   listContainer1.setItemProvider(itemProvider);
   listContainer1.setOrientation(Component.VERTICAL);
   listContainer2.setItemProvider(itemProvider2);
   listContainer2.setOrientation(Component.HORIZONTAL);
   listContainer1.setLongClickable(false);
   listContainer2.setLongClickable(false);
   listContainer1.enableScrollBar(Component.VERTICAL, true);

  2.privider的getComponent方法中添加监听
  listener = new SwipeDismissRecyclerViewTouchListener.Builder(pos, component, callbacks -> {
        ToastUtils.show("删除" + text.getText());
        list.remove(pos);
        notifyDataChanged();
    }).setBackgroundId(element, element2)
            .setIsVertical(isVertical)
            .setItemClickCallback(position -> {
                showDialog(sampleItem.getName());
            })
            .create();
    listener.setEnabled(isSwiper);
    cpt.setTouchEventListener(listener);
```
6.更多方法
```
setIsVertical(false)表示允许在水平方向上滑动

listener.setEnabled(false) 可以禁用滑动以关闭

onTouch 在未滑动的情况下对MOUSE_UP调用

onClick 当ACTION_UP在1秒内移动到物品上且移动不超过固定距离时，将被调用

通过使用setBackgroundId，您可以为项目的正常和按下状态设置背景ID
```
## 效果展示
<img src="gif/swiper.gif"></img>

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


## 版本迭代
- 1.0.2
